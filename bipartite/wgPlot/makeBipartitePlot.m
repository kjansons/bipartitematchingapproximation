M = csvread('matching.csv');
nrOfDistinct = 1000;
[m,n] = size(M);
initial = zeros(nrOfDistinct, nrOfDistinct)
coordinates = zeros(nrOfDistinct, 2)

for row=1:m
    initial(M(row,1)+1, M(row,2)+1) = M(row,3);%disp(M(row,col))
    initial(M(row,2)+1, M(row,1)+1) = M(row,3);%disp(M(row,col))
end
sparseMatching = sparse(initial);

Agroup = csvread('aside.csv');
[am,an] = size(Agroup);
Bgroup = csvread('bside.csv');
[bm,bn] = size(Bgroup);

for row=1:am+bm
    if row <= am
        coordinates(Agroup(row)+1, :) = [1, Agroup(row)+1];
    else
        coordinates(Bgroup(row-am)+1, :) = [5, Bgroup(row-am)+1];
    end
end
disp('Done');
wgPlot(sparseMatching,coordinates)

    